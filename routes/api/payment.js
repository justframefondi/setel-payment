const express = require('express')
const router = express.Router()

//payment api
router.get('/', async function(req, res) {
  const randomNumberBetweenOneAndTwo = getRandomInt(2)

  res.json({
    result: randomNumberBetweenOneAndTwo
  })
})

//subhelper for random result
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}


module.exports = router
