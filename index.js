require('dotenv').config()

const express = require('express')
const apiRoute = require('./routes/api')
const app = express()

app.use('/api', apiRoute)

app.listen(process.env.NODE_PORT, () => {
  console.log(`server running on port ${process.env.NODE_PORT}`)
})
